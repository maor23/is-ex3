<?php
class html_page {
        protected $title = "title example";
        protected $body = "body example";
        public function view(){
            echo "<html><head><title>$this->title</title></head><body><p>$this->body</p></body></html>";
        }
        function __construct($title = "",$body = ""){
            if($title != ""){
                $this->title = $title;
            }
            if($body != ""){
                $this->body = $body;
            }
        }
    }

class colored_text extends html_page {
        protected $colors = array('red','yellow','green');
        protected $color = '';
        public function __set($property,$value){
            if($property == 'color'){
                if(in_array($value,$this->colors)){
                    $this->color = $value;
                }
                else{
                    echo "<title>$this->title</title>";
                    die("the entered color is invalid - Please select one of allowed colors");
                }
            }    
        }
        public function show(){
            echo "<html><head><title>$this->title</title></head><body><p style = 'color:$this->color'> $this->body<p></body></html>";
        }
    }
?>
<?php
class font_size_text extends colored_text {
        protected $sizes = array(10,11,12,13,14,15,16,17,18,19,20,21,22,23,24);
        protected $size = '';
        public function __set($property,$value){
            parent::__set($property, $value);
            if($property == 'size'){
                if(in_array($value,$this->sizes)){
                    $this->size = $value;
                }
                else{
                    echo "<title>$this->title</title>";
                    die("the entered font-size is invalid - Please select one of allowed fonts-sizes");
                }
            }
        }

        public function show(){
            echo "<html><head><title>$this->title</title></head><body><p style = 'font-size:$this->size; color:$this->color'> $this->body<p></body></html>";
            }
    }
?>